﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.IO;
using System.Management;

namespace yrambler2001PerformanceService
{
	public partial class PerformanceService : ServiceBase
	{

		ManagementEventWatcher startWatch = new ManagementEventWatcher(
			 new WqlEventQuery("SELECT * FROM Win32_ProcessStartTrace"));

		List<string> processesToKillList = new List<string>();
		bool restartTelegram = false;

		//Process of this service.
		Process thisProcess;
		public Process ThisProcess {
			get { if (thisProcess == null) thisProcess = Process.GetCurrentProcess(); return thisProcess; }
			set => thisProcess = value;
		}

		public PerformanceService()
		{
			InitializeComponent();
			startWatch.EventArrived += new EventArrivedEventHandler(StartWatch_EventArrived);
			startWatch.Start();
		}

		//When service starts
		protected override void OnStart(string[] args)
		{
			//using (Process p = Process.GetCurrentProcess())
			//	p.PriorityClass = ProcessPriorityClass.High;
			//Thread.Sleep(6000); //debug
			int interval = 6000;
			GetOptions().ForEach(x =>
				{
					if (x.StartsWith("KillProcesses="))
					{
						var v = x.Split('=')[1];
						if (v.Length > 0)
							if (v.Contains(','))
								processesToKillList = v.Split(',').ToList();
							else processesToKillList.Add(v);
					}
					if (x.StartsWith("IntervalMsec="))
					{
						var v = x.Split('=')[1];
						if (v.Length > 0)
							try
							{
								var value = Int32.Parse(v);
								if (value != 0) interval = value;
							}
							catch (Exception) { }
					}
					if (x.StartsWith("RestartTelegram="))
					{
						var v = x.Split('=')[1];
						if (v.Contains("true")) restartTelegram = true;
					}

				});
			PriorityTelegram();
			//if any processes found in config file
			if (processesToKillList.Count > 0)
			{
				System.Timers.Timer timer = new System.Timers.Timer();
				timer.Interval = interval;
				timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
				timer.Start();
			}
		}

		//When service stops
		protected override void OnStop()
		{
			startWatch.Stop();
		}

		//On interval
		public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
		{
			//if (ThisProcess.PriorityClass != ProcessPriorityClass.High)
			//	ThisProcess.PriorityClass = ProcessPriorityClass.High;//for best result
			Kill();
		}

		//Computer wakes up
		protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
		{
			if (restartTelegram && (powerStatus == PowerBroadcastStatus.ResumeAutomatic))
				Telegram();
			return base.OnPowerEvent(powerStatus);
		}

		//Checks if a process name "s" needs to be killed
		bool IfNeedToKill(string s)
		{
			foreach (var p in processesToKillList)
				if (s.Contains(p)) return true;
			return false;
		}

		//New process
		void StartWatch_EventArrived(object sender, EventArrivedEventArgs e)
		{
			var name = e.NewEvent.Properties["ProcessName"].Value.ToString();
			if (IfNeedToKill(name)) Kill();
			if (restartTelegram && name.Contains("Telegram")) PriorityTelegram();
		}

		//Stops System lag when resuming PC, or restarting Telegram
		void PriorityTelegram()
		{
			Process.GetProcessesByName("Telegram").ToList().ForEach(x => x.PriorityClass = ProcessPriorityClass.BelowNormal);
		}

		//fix https://github.com/telegramdesktop/tdesktop/issues/3640
		void Telegram()
		{
			foreach (var process in Process.GetProcesses().Where(pr => pr.ProcessName.Contains("Telegram")))
				try
				{
					var v = process.MainModule.FileName;
					process.Kill();
					process.WaitForExit();
					LaunchAsUser.ProcessAsUser.Launch(v + " -startintray");
					PriorityTelegram();
					break;
				}
				catch (Exception) { }
		}

		//Kills Process
		void Kill()
		{
			foreach (var process in Process.GetProcesses().Where(pr => IfNeedToKill(pr.ProcessName)))
				try //try to kill process
				{
					process.Kill();
				}
				catch (Exception) { }
		}

		//Gets options from ini
		List<string> GetOptions() //read options from configuration file
		{
			var filename = AppDomain.CurrentDomain.BaseDirectory + "Configuration.ini";
			if (!File.Exists(filename))
			{
				var file = File.CreateText(filename);
				//https://techdows.com/2018/02/block-or-disable-chrome-software-reporter-tool.html not work when browser updates.
				//https://superuser.com/questions/1069174/windows-update-kb2952664-compattelrunner-exe-cannot-be-uninstalled-from-window I have experience that it still launches. 
				file.WriteLine(";KillProcesses=software_reporter_tool,CompatTelRunner");
				file.WriteLine("IntervalMsec=6000");
				file.WriteLine("KillProcesses=");
				file.WriteLine("RestartTelegram=true");
				file.Close();
			}
			return File.ReadAllLines(filename).ToList();
		}
	}
}
